modules = {
    dimple {
        resource url:'js/dimple/d3.v3.min.js'
        resource url:'js/dimple/dimple.v1.min.js'
    }

    cytoscape{
        resource url:'js/cytoscape/arbor.js'
        resource url:'js/cytoscape/cytoscape.min.js'
        resource url:'js/cytoscape/jquery.cxtmenu.min.js'
        resource url:'js/cytoscape/jquery.cytoscape-edgehandles.min.js'
        resource url:'js/cytoscape/jquery.cytoscape-panzoom.min.js'
        resource url:'js/cytoscape/jquery.cytoscape-panzoom.css'
    }
}