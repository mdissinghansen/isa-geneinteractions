navigation = {
    // Declare the "app" scope, used by default in tags
    app {
        // A nav item pointing to HomeController, using the default action
        home(controller:'home', action:'index')
        cytoscape(controller:'cytoscape', action:'example')
        dimple(controller:'dimple', action:'example')
        JSON(controller:'JSON', action:'index')
    }

}