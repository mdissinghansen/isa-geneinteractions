import geneinteractions.Gene
import geneinteractions.GeneInteraction
import geneinteractions.Genome
import geneinteractions.RResultAnswer
import grails.converters.JSON
import grails.util.DomainBuilder
import groovy.swing.factory.ListFactory

class BootStrap {

    def init = { servletContext ->

        // Registering all json objects
        JSON.registerObjectMarshaller(RResultAnswer){
            def output = [:]
            output["isSuccess"] = it.isSuccess.toString()
            output["comment"] = it.comment
            return output
        }

        JSON.registerObjectMarshaller(Gene){
            def output = [:]
            output["id"] = it.uid.toString()
            output["name"] = it.name
            return output
        }

        JSON.registerObjectMarshaller(GeneInteraction){
            def output = [:]
            output["id"] = it.uid
            output["name"] = it.name
            if(it.gene1 != null){
                output["source"] = it.gene1.toString()
            }
            if(it.gene2 != null){
                output["target"] = it.gene2.toString()
            }
            if(it.weight != null){
                output["weight"] = it.weight
            }else{
                output["weight"] = 1
            }
            return output
        }

        JSON.registerObjectMarshaller(Genome){
            def output = [:]
            output["id"] = it.uid
            output["name"] = it.name
            output["genes"] = it.genes
            output["geneInteractions"] = it.geneInteractions
            return output
        }

        int nrGenomes = 1
        int nrGenes = 2
        int nrInteractionsPrGene = 0

        for(int genomeNr = 1; genomeNr <= nrGenomes; genomeNr++){
            Genome genome = new Genome(uid: genomeNr, id: genomeNr, name: ("genome"+genomeNr))
            genome.save()
            println(String.format("init genome: \"%s\"", genome as JSON))
            for(int geneNr = 1; geneNr <= nrGenes; geneNr++){

                int geneId = Integer.parseInt(String.format("%d%05d", genomeNr, geneNr))
                String geneName = String.format("gene%d", geneId)
                Gene gene = new Gene()
                gene.setId(geneId)
                gene.setUid(geneId)
                gene.setName(geneName)
                gene.save()
                genome.addToGenes(gene)
                genome.save()
                println(String.format("init gene: \"%s\"",gene as JSON))

            }

            for(gene in genome.genes){
                for(int interactionNr = 1; interactionNr <= nrInteractionsPrGene; interactionNr++){
                    int interactionId = Integer.parseInt(gene.id.toString()+String.format("%03d",interactionNr))

                    GeneInteraction interaction = new GeneInteraction()
                    interaction.setId(interactionId)
                    interaction.setUid(interactionId)
                    interaction.setName("interaction"+interactionId)
                    interaction.setGene1(gene.id)

                    int gene2Id = Integer.parseInt(String.format("%d%05d", genome.id, Math.round(Math.random() * ( nrGenes - 1))))
                    int min = Integer.parseInt(String.format("%d%05d", genome.id, 1))
                    int max =   Integer.parseInt(String.format("%d%05d", genome.id, nrGenes))
                    while(gene2Id == gene.uid || gene2Id < min || gene2Id > max){
                        gene2Id = Integer.parseInt(String.format("%d%05d", genome.id, Math.round(Math.random() * ( nrGenes - 1))))
                    }
                    interaction.setGene2(gene2Id)
                    interaction.save()
                    genome.addToGeneInteractions(interaction)
                    genome.save()
                    println(String.format("init interaction: %s", interaction as JSON))
                }
            }
            println(genome as JSON);
        }

    }
    def destroy = {

    }
}
