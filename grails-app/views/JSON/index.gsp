<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>GI - JSON</title>
    <style>
        pre{
            white-space: -pre-wrap; /* Opera */
            white-space: -o-pre-wrap; /* Opera */
            white-space: -moz-pre-wrap; /* Mozilla, supported since 1999 */
            white-space: pre-wrap; /* CSS3 - Text module (Candidate Recommendation) http://www.w3.org/TR/css3-text/#white-space */
            word-wrap: break-word; /* IE 5.5+ */
        }
        .code{
            margin: 20px 0 20px 0;
            padding: 10px 100px 10px 50px;
            background:#efefef;
            clear:both;
            -moz-border-radius: 10px;
            border-radius: 10px;
        }

        .groovy .typ{
            color: #000043;
        }

        .groovy .str{
            color: #008000;
            font-weight: bold;
        }

        .groovy .nr{
            color: #0000FF;
            font-weight: normal;
        }

        .groovy .nr{
            color: #0000FF;
            font-weight: normal;
        }
    </style>
</head>
<body>
<a href="#page-body-full" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="page-body-full" role="main" >
    <h1>JSON</h1>
    <p>To ease the conversion from groovy-class object to JSON, there was used a so-called JSON object marshaller.
    This was achieved by adding the following piece of code in the "grails-app/conf/BootStrap.groovy" file.</p>

<pre class="code groovy">
// Registering a JSON object
JSON.registerObjectMarshaller(Gene){
    <strong class="var">def</strong> output = [:]
    output[<b class="str">"id"</b>] = it.uid.toString()
    output[<b class="str">"name"</b>] = it.name
    <strong class="var">return</strong> output
}
</pre>

    <p>Once this was done for each class that should be rendered as a JSON would then be called with "as JSON" after it. The
    example below is from the JSONController/GenomeAsJSON method:</p>

<pre class="code groovy">
def GenomeAsJSON(int id) {
    <strong class="var">def</strong> genome = Genome.get(id)
    <strong class="var">if</strong>(genome != <strong class="var">null</strong>){
        render genome as JSON
    }<strong class="var">else</strong>{
        <strong class="var">render</strong>(status: <b class="nr">503</b>, text:
            <b class="str">"Failed to retrieve genome."</b> +
            <b class="str">"This page is made as a JSON response to retrieve a genome JSON object, "</b> +
            <b class="str">"with the id of the genome as parameter."</b>)
    }
}
</pre>

<p>
    Thus, as can be seen in the code snippet above, the return statement needed to return a JSON object is just "render {object} as JSON" (in this example "genome" is the object).
</p>



</div>

</body>
</html>