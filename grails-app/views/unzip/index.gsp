<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Unzipper</title>
</head>
<body>
<a href="#page-body-full" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="page-body-full" role="main" >
    <h1>Unzip package</h1>

    <g:if test="${flash.error}">
        <div class="errors" role="alert">&nbsp;${flash.error}</div>
        ${flash.error = ''}
    </g:if>
    <g:if test="${flash.okay}">
        <div class="message" role="status">${flash.okay}</div>
        ${flash.okay = ''}
    </g:if>

    <g:uploadForm>
        <table>
            <tr>
                <td>Zipped file:</td>
                <td><input type="file" name="zippedFile" style="width:400px;" /> </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" /></td>
            </tr>

        </table>
    </g:uploadForm>


</div>

</body>
</html>