<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>GI - dimple</title>
    <r:require module="dimple"/>
    <style>

    .node {
        stroke: #fff;
        stroke-width: 1.5px;
    }

    .link {
        stroke: #999;
    }

    </style>
</head>
<body>
<a href="#page-body-full" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="page-body-full" role="main" >
    <h1>D3 network visualization</h1>
    The D3.js JavaScript library is used for advanced visualization of network/graphs, using a JSON-object as input.<br/><br/>
    <b>Implemented version: </b> <i>d3.v3</i><br/><br/>
    <b>Link to website: </b> <i><a href="http://d3js.org/">http://d3js.org/</a></i>

    <fieldset class="embedded">
        <legend>Example</legend>

        <input type="button" class="buttons" value="Draw graph" id="drawGraphButton"/> <br/>

        <div id="d3Graph"></div>

        <script>
            $(document).ready(function () {
                $("#drawGraphButton").click(function(){
                    $.getJSON("<g:createLink controller="JSON" action="GenomeAsJSON" id="1" absolute="true"></g:createLink>", drawD3Graph);

                });
            });

        function drawD3Graph(data){
            $("#d3Graph").empty();

            var graph = {"nodes":[], "links":[]};
            var nodeIndex = {};
            // Creating the node stack (genes)
            $.each(data.genes, function(index, gene){
                //var node  = { id: gene.id.toString(), name: gene.name.toString() };
                var node = {"x":0, "y":0};
                nodeIndex[gene.id.toString()] = index;
                graph.nodes.push(node);
            });

            // Creating the edge stack (interactions)
            var edgeStack = [];
            $.each(data.geneInteractions, function(index, interaction){
                var edge = { "source": nodeIndex[interaction.source.toString()], "target": nodeIndex[interaction.target.toString()], "value": 1  };
                graph.links.push(edge);
            });


            var width = 900, height = 500;
            var color = d3.scale.category20();

            var force = d3.layout.force()
                    .charge(-120)
                    .linkDistance(30)
                    .size([width, height]);

            var svg = d3.select("#d3Graph").insert("svg")
                    .attr("width", width)
                    .attr("height", height);

            force.nodes(graph.nodes)
                        .links(graph.links)
                        .start();

                var link = svg.selectAll(".link")
                        .data(graph.links)
                        .enter().append("line")
                        .attr("class", "link")
                        .style("stroke-width", function(d) { return Math.sqrt(d.value); });

                var node = svg.selectAll(".node")
                        .data(graph.nodes)
                        .enter().append("circle")
                        .attr("class", "node")
                        .attr("r", 5)
                        .style("fill", function(d) { return color(d.group); })
                        .call(force.drag);

                node.append("title")
                        .text(function(d) { return d.name; });

                force.on("tick", function() {
                    link.attr("x1", function(d) { return d.source.x; })
                            .attr("y1", function(d) { return d.source.y; })
                            .attr("x2", function(d) { return d.target.x; })
                            .attr("y2", function(d) { return d.target.y; });

                    node.attr("cx", function(d) { return d.x; })
                            .attr("cy", function(d) { return d.y; });
                });

            /*
            var svg = d3.select("#d3Graph").insert("svg")
                    .attr("width", width)
                    .attr("height", height);

            graph.links.forEach(function(d) {
                d.source = graph.nodes[d.source];
                d.target = graph.nodes[d.target];
            });

            var link = svg.append("g")
                    .attr("class", "link")
                    .selectAll("line")
                    .data(graph.links)
                    .enter().append("line");
                    .attr("x1", function(d) { return d.source.x; })
                    .attr("y1", function(d) { return d.source.y; })
                    .attr("x2", function(d) { return d.target.x; })
                    .attr("y2", function(d) { return d.target.y; });

            var node = svg.append("g")
                    .attr("class", "node")
                    .selectAll("circle")
                    .data(graph.nodes)
                    .enter().append("circle")
                    .attr("r", 4)
                    .attr("cx", function(d) { return d.x; })
                    .attr("cy", function(d) { return d.y; })
                    .call(d3.behavior.drag()
                            .origin(function(d) { return d; })
                            .on("drag", function(d) {
                                d.x = d3.event.x, d.y = d3.event.y;
                                d3.select(this).attr("cx", d.x).attr("cy", d.y);
                                link.filter(function(l) { return l.source === d; }).attr("x1", d.x).attr("y1", d.y);
                                link.filter(function(l) { return l.target === d; }).attr("x2", d.x).attr("y2", d.y);
                            }));*/

        }
        </script>

    </fieldset>
</div>

</body>
</html>