<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>GI - Cytoscape</title>

    <r:require module="cytoscape"/>

</head>
<body>
<a href="#page-body-full" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div id="page-body-full" role="main" >
    <h1>Cytoscape network visualization</h1>
    The javascript version of Cytoscape allows for simple visualization of a network, using a JSON-object as input.<br/><br/>
    <b>Implemented version: </b> <i>2.0.2</i><br/><br/>
    <b>Link to website: </b> <i><a href="http://cytoscape.github.io/cytoscape.js/">http://cytoscape.github.io/cytoscape.js/</a></i>

    <fieldset class="embedded">
        <legend>Example</legend>

        <input type="button" class="buttons" value="Draw graph" id="drawGraphButton"/>

        <div id="cy" name="cy" style="width:800px;height:800px;"></div>

        <script>
            $(document).ready(function () {

                $("#drawGraphButton").click(function(){
                    $.getJSON("<g:createLink controller="JSON" action="GenomeAsJSON" id="1" absolute="true"></g:createLink>", cytoscapeCallBackWithData);
                });
            });

            // The method that is called once the JSON data has been returned.
            // Is used to draw the cytoscape network graph
            function cytoscapeCallBackWithData(data){

                // Creating the node stack (genes)
                var nodeStack = [];
                $.each(data.genes, function(index, gene){
                    var node  = { data: { id: gene.id.toString(), name: gene.name.toString() } };
                    nodeStack.push(node);
                });

                // Creating the edge stack (interactions)
                var edgeStack = [];
                $.each(data.geneInteractions, function(index, interaction){
                    var edge = { data: { source: interaction.source.toString(), target: interaction.target.toString()} };
                    edgeStack.push(edge);
                });

                $('#cy').empty();

                // Calling the cytoscape functions with parameters
                $('#cy').cytoscape({
                    style: cytoscape.stylesheet()
                            .selector('node')
                            .css({
                             //   'content': 'data(name)',
                                'text-valign': 'center',
                                'color': 'white',
                                'width': 10,
                                'height': 10//,
                               // 'text-outline-width': 2,
                               // 'text-outline-color': '#888'
                            })
                            .selector('edge')
                            .css({
                                //'target-arrow-shape': 'triangle'
                            })
                            .selector(':selected')
                            .css({
                                'background-color': 'black',
                                'line-color': 'black',
                                'target-arrow-color': 'black',
                                'source-arrow-color': 'black'
                            })
                            .selector('.faded')
                            .css({
                                'opacity': 0.25,
                                'text-opacity': 0
                            }),

                    elements: {
                        nodes: nodeStack,
                        edges: edgeStack
                    },

                    ready: function(){
                        window.cy = this;

                        cy.elements().unselectify();

                        cy.on('tap', 'node', function(e){
                            var node = e.cyTarget;
                            var neighborhood = node.neighborhood().add(node);

                            cy.elements().addClass('faded');
                            neighborhood.removeClass('faded');
                        });

                        cy.on('tap', function(e){
                            if( e.cyTarget === cy ){
                                cy.elements().removeClass('faded');
                            }
                        });

                        cy.fit();
                    }
                });
            }
        </script>
    </fieldset>
</div>

</body>
</html>