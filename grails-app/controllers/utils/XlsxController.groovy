package utils

import org.springframework.web.multipart.MultipartHttpServletRequest

class XlsxController {
    def xlsxService

    def index() {
        if(request.getMethod() == 'POST'){
            // Have to convert request into file request
            MultipartHttpServletRequest r = (MultipartHttpServletRequest) request

            // Grab file and check it exists
            def dataFile = r.getFile("excelFile")

            if (dataFile.empty) {
                flash.error = 'A file has to be chosen.'
                return
            }

            String suffixWithDot = dataFile.originalFilename.substring(dataFile.originalFilename.lastIndexOf("."))

            if(suffixWithDot != ".xls" && suffixWithDot != ".xlsx"){
                flash.error = 'The file has to be in excel format.'
                return
            }

            xlsxService.parse(dataFile)
        }
    }
}
