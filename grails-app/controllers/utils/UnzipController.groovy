package utils

import org.springframework.web.multipart.MultipartHttpServletRequest

class UnzipController {
    def unzipService
    def xlsxService

    def index() {
        if(request.getMethod() == 'POST'){
            // Have to convert request into file request
            MultipartHttpServletRequest r = (MultipartHttpServletRequest) request

            // Grab file and check it exists
            def dataFile = r.getFile("zippedFile")
            if (dataFile.empty) {
                flash.error = 'A file has to be chosen.'
                render(view: 'index')
                return
            }

            def unpacked = unzipService.Unpack(dataFile.getInputStream())
            unpacked.keySet().each {key ->
                println("-\nParsing " + key)
                xlsxService.parse(unpacked.get(key))
            }
            println("-\nFinished!")
        }
    }
}
