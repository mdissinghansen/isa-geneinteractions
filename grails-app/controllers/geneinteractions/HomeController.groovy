package geneinteractions

class HomeController {
    static navigation = false
    def index() {
        redirect(uri:'/')
    }
}
