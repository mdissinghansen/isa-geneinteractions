package geneinteractions

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class RResultController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def scaffold = true

    def saveResult()   {

        println("---------")
        println("JSON-object:")
        def jsonObj = JSON.parse(request.getParameter("res") )
        println(jsonObj)
        println("--")
        RResult res = new RResult(jsonObj);
        println("RResult-object:")
        println(String.format("%s, %s", res.uid, res.name));
        println("--")
        println("Values:")

        List parsedList = JSON.parse(request.getParameter("vals"))

        // Convert from a list of JSONObject to a list of Foo
        def entries = parsedList.collect {JSONObject jsonObject ->
            new RResultTuple(entry: jsonObject.get("entry"), value: jsonObject.get("val"))
        }

        // you also need to add the entries to the RResult object, and don't forget to save
        entries.each{
            res.addToEntries(it)
        }
        res.save(flush: true)

        if(entries == null){
            println("No entries found.")
        }   else{
            println(entries.size())}

        for(def i = 0; i < entries.size(); i++)
        {
            println(String.format("Entry = \"%s\" ; Value = \"%s\"", entries[i].entry, entries[i].value))
        }
        println("---------")

        //this wouldn't end up in the database since you don't call .save()
        RResultAnswer answer = new RResultAnswer();
        answer.comment = "The save was a success";
        answer.isSuccess = true;

        render answer as JSON;
    }
}
