package geneinteractions

import grails.converters.JSON

class JSONController {
    static navigation = false

    def index(){}

    def GenomeAsJSON(int id) {
        def genome = Genome.get(id)
        if(genome != null){
            render genome as JSON
        }else{
            render(status: 503, text:
                    "Failed to retrieve genome. <br/>" +
                    "This page is made as a JSON response to retrieve a genome JSON object, "+
                    "with the id of the genome as parameter.")
        }
    }
}
