package geneinteractions

class RResult {
    long id
    long uid
    String name

    static hasMany = [entries: RResultTuple]

    static constraints = {
        id unique:  true
        uid unique:  true
        name nullable: true, blank: false, unique: true
    }
}
