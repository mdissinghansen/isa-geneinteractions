package geneinteractions

class Genome {
    int id
    long uid
    String name
    static hasMany = [genes: Gene, geneInteractions: GeneInteraction]

    static constraints = {
        id unique:  true
        uid unique:  true
        name nullable: true, blank: false, unique: true
    }

    static mapping = {

    }

    String toString(){
        name?:(id + "(" + Calendar.getInstance().getTime() + ")")
    }
}
