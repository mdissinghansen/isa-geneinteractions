package geneinteractions

class Gene {
    int id
    long uid
    String name

    static belongsTo = [genome: Genome]

    static constraints = {
        id unique:  true
        uid unique:  true
        name nullable: true, blank: false, unique: true
    }

    static mapping = {

    }
}
