package geneinteractions

class GeneInteraction {
    int id
    long uid
    int gene1
    int gene2
    double weight
    String name

    static belongsTo = [ownerGenome: Genome]

    static constraints = {
        id unique:  true
        uid unique: true
        name nullable: true, blank: false, unique: true
        gene1 nullable: true
        gene2 nullable: true
        weight nullable: true
    }

    static mapping = {

    }
}
