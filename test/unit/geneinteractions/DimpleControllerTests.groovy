package geneinteractions



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(DimpleController)
class DimpleControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}
