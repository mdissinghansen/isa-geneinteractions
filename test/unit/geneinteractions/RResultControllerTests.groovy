package geneinteractions



import org.junit.*
import grails.test.mixin.*

@TestFor(RResultController)
@Mock(RResult)
class RResultControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/RResult/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.RResultInstanceList.size() == 0
        assert model.RResultInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.RResultInstance != null
    }

    void testSave() {
        controller.save()

        assert model.RResultInstance != null
        assert view == '/RResult/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/RResult/show/1'
        assert controller.flash.message != null
        assert RResult.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/RResult/list'

        populateValidParams(params)
        def RResult = new RResult(params)

        assert RResult.save() != null

        params.id = RResult.id

        def model = controller.show()

        assert model.RResultInstance == RResult
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/RResult/list'

        populateValidParams(params)
        def RResult = new RResult(params)

        assert RResult.save() != null

        params.id = RResult.id

        def model = controller.edit()

        assert model.RResultInstance == RResult
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/RResult/list'

        response.reset()

        populateValidParams(params)
        def RResult = new RResult(params)

        assert RResult.save() != null

        // test invalid parameters in update
        params.id = RResult.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/RResult/edit"
        assert model.RResultInstance != null

        RResult.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/RResult/show/$RResult.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        RResult.clearErrors()

        populateValidParams(params)
        params.id = RResult.id
        params.version = -1
        controller.update()

        assert view == "/RResult/edit"
        assert model.RResultInstance != null
        assert model.RResultInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/RResult/list'

        response.reset()

        populateValidParams(params)
        def RResult = new RResult(params)

        assert RResult.save() != null
        assert RResult.count() == 1

        params.id = RResult.id

        controller.delete()

        assert RResult.count() == 0
        assert RResult.get(RResult.id) == null
        assert response.redirectedUrl == '/RResult/list'
    }
}
